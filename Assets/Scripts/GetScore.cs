﻿using UnityEngine;
using UnityEngine.UI;

public class GetScore : MonoBehaviour {
    Text text_score;
    int score;

    void Start () {
        score = PlayerPrefs.GetInt("best_score", score);
        text_score = GetComponent<Text>();
        text_score.text = score.ToString(); 
    }

}
