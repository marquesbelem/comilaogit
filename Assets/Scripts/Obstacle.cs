﻿using UnityEngine;

public class Obstacle : MonoBehaviour {

    Rigidbody2D rb2d;
    public Vector2 speed = new Vector2( -4, 0);

    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
    }	

	void Update () {
        rb2d.velocity = speed;

        Vector2 screen_position = Camera.main.WorldToScreenPoint(transform.position);

        if (screen_position.x < 0)
            Debug.Log("saiu");
    }
}
