﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    Rigidbody2D rb2d;
    public Vector2 jump = new Vector2(0, 300);
    bool game_over;
    public GameObject panel_game_over;
    bool small, large;

    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update () {
        if (Input.GetKeyUp("space")) {

            if (!game_over)  {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(jump);
            }
            else {
                SceneManager.LoadScene("game");
            }
        }

        Vector2 screen_position = Camera.main.WorldToScreenPoint(transform.position);

        if (screen_position.y > Screen.height || screen_position.y < 0)
            GameOver();
	}

   void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.tag == "Obstacle")
             GameOver();

        if (collision.gameObject.tag == "PowerUp")
             Eat(collision.gameObject);
    }
   
    void GameOver() {
        game_over = true;
        panel_game_over.SetActive(true);
        Spawn._intance.CancelInvoke("SpawnObstacle");
        Spawn._intance.CancelInvoke("SpawnPowerUp");
        Spawn._intance.CancelInvoke("Score");
    }

    void Eat(GameObject g) {
        print("comeu");
        Destroy(g);
    }
}
