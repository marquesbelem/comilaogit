﻿using UnityEngine;
using UnityEngine.UI;

public class Spawn : MonoBehaviour {

    public static Spawn _intance = null;
    public GameObject group_obstacle;
    public Transform start_spawn;
    public GameObject power_up; 

    int score = 0;
    public int best_score;
    public Text text_score;

    Vector2 new_start;
    void Awake() {
        if(_intance == null) 
            _intance = this;
        
    }

    void Start () {
        best_score = PlayerPrefs.GetInt("best_score", best_score);
        InvokeRepeating("SpawnObstacle", 1f, 5f);
        InvokeRepeating("SpawnPowerUp", 2f, 1f);
        InvokeRepeating("Score", 1f, 1.5f);
    }

    void SpawnObstacle() {
        new_start = new Vector2(start_spawn.position.x, Random.Range(-1.68f, -5.68f));
        Instantiate(group_obstacle, new_start, Quaternion.identity);
    }

    void SpawnPowerUp() {
      //  Instantiate(power_up, start_spawn.position, Quaternion.identity);
    }

    void Score() {
        score++;

        if(score > best_score) {
            best_score = score;
            PlayerPrefs.SetInt("best_score", best_score);
        }

        text_score.text = score.ToString();
    }

}
