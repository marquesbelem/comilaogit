﻿using UnityEngine;

public class StartGame : MonoBehaviour {
    
    public GameObject player;
    public GameObject panel_start_game;

    void Update () {

        if (Input.GetKeyUp("space")) {
            Spawn._intance.enabled = true;
            player.SetActive(true);
            panel_start_game.SetActive(false);
            this.enabled = false;
        }
    }
}
